package lib;
import ast.*;

public class FOOLlib {
	
	//valuta se il tipo "a" � <= al tipo "b", dove "a" e "b" sono tipi di base: int o bool
	public static boolean isSubtype(Node a, Node b) { //aggiungere il caso in cui uno e booleano e l'altro intero
		if (a instanceof BoolTypeNode && b instanceof IntTypeNode) {
			return true;
		} else {
			return a.getClass().equals(b.getClass());	
		}
	
	}

}
