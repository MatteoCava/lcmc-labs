package ast;

public class BoolTypeNode implements Node {

	public BoolTypeNode() {
	}

	public String toPrint(String s) {
		return s + "BoolType\n";
	}

	//in realt� nei nodi type il typecheck non viene MAI chiamato
	public Node typeCheck() {
		return null;
	}

}