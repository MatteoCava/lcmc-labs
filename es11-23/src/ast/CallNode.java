package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class CallNode implements Node {

	private String id;
	private STentry entry;
	private ArrayList<Node> parlist = new ArrayList<Node>();

	public CallNode(String i, STentry st, ArrayList<Node> p) {
		this.id = i;
		this.entry = st;
		this.parlist = p;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}

		return s + "Call:" + id + "\n" + entry.toPrint(s + "  ") + parlstr;
	}

	// per casa, permettere l'utilizzo di booleani al posto di interi, cambiare
	// quindi la isSubtype
	public Node typeCheck() {
		if (!(entry.getType() instanceof ArrowTypeNode)) {
			System.out.println("Invocation of a non function " + this.id);
			System.exit(0);
		}
		ArrowTypeNode node = (ArrowTypeNode) entry.getType();
		ArrayList<Node> entryParlist = node.getParlist();
		if (node.getParlist().size() != this.parlist.size()) {
			System.out.println("Wrong number of parameters in the invocation of " + this.id);
			System.exit(0);
		}

		for (int i = 0; i < entryParlist.size(); i++) {
			Node entryNode = entryParlist.get(i);
			Node useNode = this.parlist.get(i);
			if (!FOOLlib.isSubtype(useNode.typeCheck(), entryNode)) {
				System.out.println("Wrong type for " + i + "-th parameter in the invocation of " + this.id);
				System.exit(0);
			}
		}
		return node.getRet();
	}

	/*
	 * ESERC: regola (con subtyping) vista a lezione / regola di inferenza per le
	 * chiamate a funzione, prende il tipo della funzione (il tipo freccia della
	 * funzione che trovo nella entry.type ) recupero tipo (deve essere
	 * ArrowTypeNode) da STentry e controllare che sia un arrowtypenode /controllare
	 * che il numero di parametri formali sia uguale alle espressioni che ho come
	 * figlio, controllare poi i tipi dei parametri e che le espressioni con cui lo
	 * 
	 * errori possibili (che indicano, in ordine, i controlli da fare):
	 * "Invocation of a non-function "+id
	 * "Wrong number of parameters in the invocation of "+id
	 * "Wrong type for "+i+"-th parameter in the invocation of "+id leggere tipo
	 * parametri e tipo di ritorno da un ArrowTypeNode tramite i suoi metodi
	 * getRet() e getParList() (crearli) dopo i check ritorno il tipo di ritorno �
	 * il tipo di ritorno dentro l'arrow type node
	 * 
	 */

}