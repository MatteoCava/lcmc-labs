package ast;

public class STentry {

	private Node type;
	private int nl;

	public STentry(int n) {
		nl = n;
	}

	public STentry(int nl, Node type) {
		this.type = type;
		this.nl = nl;
	}
	
	public void addType(Node type) {
		this.type = type;
	}
	
	public Node getType() {
		return this.type;
	}

	public String toPrint(String s) {
		return s + "STentry: nestlev " + Integer.toString(nl) + "\n" + 
				s + "STentry: type\n" + 
				type.toPrint(s+" ");
	}

}