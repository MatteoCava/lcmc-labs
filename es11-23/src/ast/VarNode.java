package ast;

import lib.FOOLlib;

public class VarNode implements Node {

	private String id;
	private Node type;
	private Node exp;

	public VarNode(String i, Node t, Node v) {
		id = i;
		type = t;
		exp = v;
	}

	public String toPrint(String s) {
		return s + "Var:" + id + "\n" 
				+ type.toPrint(s + "  ")
				+ exp.toPrint(s + "  ");
	}

	public Node typeCheck() { 
		if (!FOOLlib.isSubtype(exp.typeCheck(), type)) {//ho exp che � espressione con cui inizializzo la var, devo controllarne il tipo e controllare che sia corretto
			System.out.println("Incompatible value for variabile: " + id); //ho dato alla variabile un valore sbagliato rispetto al suo tipo
			System.exit(0);
		}
		return null;
	}

}