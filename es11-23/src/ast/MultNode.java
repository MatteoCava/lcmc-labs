package ast;

import lib.FOOLlib;

public class MultNode implements Node {

  private Node left;
  private Node right;
  
  public MultNode (Node l, Node r) {
   left=l;
   right=r;
  }
  
  public String toPrint(String s) {
   return s+"Mult\n" + left.toPrint(s+"  ")  
                     + right.toPrint(s+"  ") ; 
  }

  public Node typeCheck() {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode()) && // voglio che i tipo che vinee ritornato sia sottotipo di intero
				FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()))) {
			System.out.println("Non integer in multiplication");
			System.exit(0);
		}

		return new IntTypeNode();
	}
  
}  