package ast;

import lib.FOOLlib;

public class PlusNode implements Node {

	private Node left;
	private Node right;

	public PlusNode(Node l, Node r) {
		left = l;
		right = r;
	}

	public String toPrint(String s) {
		return s + "Plus\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	public Node typeCheck() {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode()) && // voglio che i tipo che vinee ritornato sia sottotipo di  intero
				FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()))) {
			System.out.println("Non integer in sum");
			System.exit(0);
		}

		return new IntTypeNode();
	}

}