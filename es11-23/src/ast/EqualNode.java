package ast;

import lib.FOOLlib;

public class EqualNode implements Node {

	private Node left;
	private Node right;

	public EqualNode(Node l, Node r) {
		left = l;
		right = r;
	}

	public String toPrint(String s) {
		return s + "Equal\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	//se ho oggetti diversi l'uguaglianza di pu� avere con l'ereditariet� multipla es eq(A a, B b) se passo due C che eredita da entrambi allora funge (in java di ottiene con le inteface)
	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) ||  FOOLlib.isSubtype(r, l))) {
			System.out.println("Incompatible types in equals");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	
	//in generale si prende il discendente comune pi� alto 
}