package ast;

import lib.FOOLlib;

public class IfNode implements Node {

	private Node cond;
	private Node th;
	private Node el;

	public IfNode(Node c, Node t, Node e) {
		cond = c;
		th = t;
		el = e;
	}

	public String toPrint(String s) {
		return s + "If\n" + cond.toPrint(s + "  ") + th.toPrint(s + "  ") + el.toPrint(s + "  ");
	}

	public Node typeCheck() {
		if (!FOOLlib.isSubtype(cond.typeCheck(), new BoolTypeNode())) {
			System.out.println("Non boolean condition in if");
			System.exit(0);
		}
		Node t = th.typeCheck();
		Node e = el.typeCheck();
		if (FOOLlib.isSubtype(t, e)) {
			return e;
		}
		if (FOOLlib.isSubtype(e, t)) {
			return t;
		}
		System.out.println("Incopatible types in then-else brances");
		System.exit(0);
		return null;
		
	}
	
	// if cond then new A() else new B()  quello che viene tornato � il genitore comunune, usando solo bool e int devo controllare che uno sia il sottotipo dell'altro e tornare il pi� grande

}