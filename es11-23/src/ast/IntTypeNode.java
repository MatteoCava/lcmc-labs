package ast;

public class IntTypeNode implements Node {

	public IntTypeNode() {
	}

	public String toPrint(String s) {
		return s + "IntType\n";
	}

	//in realt� nei nodi type il typecheck non viene MAI chiamato
	public Node typeCheck() {
		return null;
	}

}