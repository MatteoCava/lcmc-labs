package ast;

import java.util.ArrayList;

public class ProgNode implements Node { //nodo radice quando non ho il letin globale

	private Node exp;

	public ProgNode(Node e) {
		exp = e;
	}

	public String toPrint(String s) {
		return s + "Prog\n" + exp.toPrint(s + "  ");
	}

	public Node typeCheck() { //come progletintnode ma senza dichairazione
		return exp.typeCheck();
	}

}