package ast;

import java.util.ArrayList;

public class ArrowTypeNode implements Node { // rappresetazione strutturato fatto da una composizione di tipi
												// (T1,T2,...,Tn)->T

	private ArrayList<Node> parlist; // (T1,T2,...,Tn)
	private Node ret; // T

	public ArrowTypeNode(ArrayList<Node> p, Node r) {
		parlist = p;
		ret = r;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}

		return s + "ArrowTypeNode\n" + parlstr + ret.toPrint(s + "  ->");
	}
	
	public ArrayList<Node> getParlist() {
		return this.parlist;
	}
	
	public Node getRet() {
		return this.ret;
	}

	//in realt� nei nodi che rappresentano tipi il typecheck non viene MAI chiamato
	public Node typeCheck() {
		return null;
	}

}
