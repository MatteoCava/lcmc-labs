Tutti i parser top down non lavorano con la ricorsione sx che va quindi eliminata

antlr quando crea la classe java per ogni variabile della grammatica crea un metodo di java, se ci metto una ricorsione a sx si genererebbe un loop infinito.

i parser top down sono più facili da capire, le grammatiche in antlr sono facili da capire per chi se ne intende poco.

11-06 VIRTUAL MACHINE
per semplicità invece che far produrre codice binario al compilatore, gli facciamo compilare codice assembler in un file di testo. 

l'assemblatore convertirà poi li tutto in binario, creo un immenso codice oggetto fatto solo di numeri che arriverà al processore o a una virtual machine
noi faremo un ciclo while che dice cosa fare in base all'istruzione

in questa lezione viene fatto tutto l'assemblatore

si chiama stack virtual machine perchè per fare i calcoli si appoggia su uno stack.


- lo schema per saltare a una subroutine e tornare indieto è fare una JS per salatare a una subrutine (verra salvato l'indirizzo attuale su RA e poi si va alla subroutine), per tornare indietro si fa una LOADRA e un'altra JS

- importanti nel strutture labelAddress e labelRef, una che mi tiene le dichiarazione delle etichette e l'altra i buchi

- lo stack si usa per le variabili e le strutture dati locali

- loadw e storew: indirizzamento indirretto, non specifico quindi direttamente i valori ma l'indirizzo
	store sarebbe un salvataggio indiretto: ho un valore sullo stack e voglio salvarlo nella memria specificando un indirizzo, devo speficicare quindi il valore che metto e l'indirizzo a cui metterlo
	
- JS jump to subroutine: diversa dal jump
	prende l'indirizzo a cui saltare dalla cima dello stack
	lui mette in RA(return address) l'istruzione successiva
	in questa maniera posso tornare al da cui avevo saltato
	si torna indietro grazie a LOADRA e STORERA: dopo essere saltato e avere caricato l'indirizzo successivo su RA, con la LOADRA rimetto quell'indirizzo sulla cima dello stack e faccio un'altra js per rirotnare in quel punto
	
	
############################################
	
9-11 compilatore

#########################
16-11 implementazione symbol table
aggiunta dichairazione variabili e identificatori e dichairazione di funzioni senza paramentri
realizzazione lista di tabelle come symbol table, capendo l'uso di variabile a quale dichiarazione corrisponde 

unico uguale che ho è quello di inizializzazione, la variabile successivamente non può più essre valorizzata
idem con gli oggetti, una volta creati i loro campi non si possono più cambiare


fase top-down senza controllare i tipi


assumiamo paramentri e ambiente locale della funzione essere nello stesso scope

declist non ritorna un nodo ma una lista di nodi
ProgLetInNode che contiene declist e il node di prima  


variabile globale che contiene il nesting level corrente, quando entriamo in uno scope la incrementiamo, quando usciamo la decrementiamo
quando incrontriamo una DICHIARAZIONE aggiungiamo una nuova entry nel fronte dello scope locale, controllando prima quell'identiicatore non ci sia già
viene aggiunta in una tabella fatta come hash map stringa(idenficatore) a oggetti che sono di una classe chiamata stentry, dove ci soono info (all'inizio solo sul nesting level)
quando incontriamo un USO controlliamo se c'è nella tabella, se c'è creiamo l'id node, se non c'è proviamo nella tabella successiva fino ad arrivare all'ultima tabella che rappresenta lo scope globale, se non c'è vuol dire che l'identificatore non è dichiarato

IL FRONTE È LA MAPPA IN POSIZIONE NESTING LEVEL 
quando ricerco parto dalla tabella in posizione nesting level, se non c'è vado a posizioni inferiori decrementando l'indice

stentry NON è un nodo MA una decorazione

sta lista di tabelle è dinamica: quando entro in un livello ne aggiungo una, quando esco la rimuovo - alla fine del parsing non mi servirà più 

entro in un altro scope quando arrivo ai parametri di una funzione: assumiamo che i parametri e le var locali di una funzione stiano nello stesso scope!

per comodità ecco senza il codice java in mezzo a far casino:

prog : exp SEMIC                        
     | LET declist IN exp SEMIC
     ;

declist : (
            (   VAR ID COLON type ASS exp
              | FUN ID COLON type LPAR (ID COLON type (COMMA ID COLON type)* )? RPAR (LET declist IN)? exp 
            ) SEMIC 
          )+
        ;
		
quando trovo un uso devo cercare la dichiarazioneee
PROSSIMA VOLTA SI ARRICCHISCONO PALLINI CON INFORMAZIONI SUL TIPO: TYPECHECKING

