package ast;

public class EqNode implements Node {
	
	private Node left;
	private Node right;

	public EqNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Eq\n" + 
				left.toPrint(indent + "  ") +
				right.toPrint(indent + "  ");
	}

}
