package ast;

public class IfNode implements Node {
	
	private Node condNode;
	private Node thenNode;
	private Node elseNode;

	public IfNode(Node condNode, Node thenNode, Node elseNode) {
		this.condNode = condNode;
		this.thenNode = thenNode;
		this.elseNode = elseNode;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "If\n" + 
				condNode.toPrint(indent + "  ") +
				thenNode.toPrint(indent + "  ") +
				elseNode.toPrint(indent + "  ");
	}

}
