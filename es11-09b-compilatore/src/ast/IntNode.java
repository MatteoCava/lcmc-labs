package ast;

public class IntNode implements Node {
	
	private int val;

	public IntNode(int val) {
		this.val = val; 
	}
	
	@Override
	public String toPrint(String indent) {
		return indent + "Int: " + val + "\n" ;
	}

}
