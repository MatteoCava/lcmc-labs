package ast;

public class BoolNode implements Node {
	
	private boolean val;

	public BoolNode(boolean val) {
		this.val = val;
	}
	
	@Override
	public String toPrint(String indent) {
		return indent + "Bool: " + val + "\n" ;
	}

}
