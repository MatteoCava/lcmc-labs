package ast;

public class ProgNode implements Node {

	private Node exp;

	public ProgNode(Node exp) {
		this.exp = exp;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Prog\n" + exp.toPrint(indent + "  ");
	}

}
