grammar SimpleExpRight;
  
//viene inserita nella classe lexer generata
@lexer::members {
	int lexicalErros=0;
}


prog : exp { System.out.println("Parsing finished!");/*qui posso mettere pezzo java da usare quando l'input � stato matchato */} ;

/* 
E  ->  T E'
E' -> + E | \epsilon
T  ->  V T'
T' -> * T | \epsilon
V  -> n | ( E )

* */

exp : term (PLUS exp)? ; //l'operatore ? semplifica la grammatica, evita di inserire exp2
//exp2 : (PLUS exp)? ; //in EBNF c'� modo di mettere una stringa oppure epsilon

term : value (TIMES term)? ;
//term2 : TIMES term | ; anche qui posso eliminarlo
value : NUM | LPAR exp RPAR ;



// LEXER RULES, devo definire per ogni tokern lessema che metcha l'input

PLUS	: /*espressione regolare che mi dice lessema matchato con token */ '+'; 
TIMES	: '*';
LPAR	: '(';
RPAR	: ')';
NUM		: ('1'..'9')('0'..'9')* | '0'; /* in antlr gli intervalli si rappresentano con i .. */

WHITESP	: (' ' | '	' | '\n' | '\r' )+ -> channel(HIDDEN); //il parser non deve vedere il token whitespace 

// in questo caso viene accettata anche sta roba /*blablba*/ codice /*blabla*, non devo applicare la regola di maximal match ma appena posso devo uscire, lo specifico col ?/ 
// COMMENT	: '/*' (.)* '*/' -> channel(HIDDEN);
COMMENT	: '/*' (.)*? '*/' -> channel(HIDDEN);

/* gestione degli errori, da fare con un token speciale che metcha qualsiasi cosa 
 * . metcha un carattere che non � matchato da nessun altro, gli associo un'azione dopo che ha riconosciuto,
 * incremento variabile globale 
 */
ERR		: . {System.out.println("Invalid char: "+ getText()); lexicalErros++;} -> channel(HIDDEN); 
