import java.io.*;
import org.antlr.v4.runtime.*;

//SimpleExpLexer
//SimpleExpParser
//CommonTokenStream
//lexicalErrors
//getNumberOfSyntaxErrors

public class Test {
    public static void main(String[] args) throws Exception {

        String fileName = "prova.txt";
     
        CharStream chars = CharStreams.fromFileName(fileName);  //dal file creiamo uno stream di caratteri che poi daremo in pasto 
        SimpleExpLexer lexer = new SimpleExpLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer); //rappresenta il flusso di token in uscita dal lexer
        SimpleExpParser parser = new SimpleExpParser(tokens);
        parser.prog(); //la variabile iniziale che � diventata un metodo
        
        System.out.println("You had: " + lexer.lexicalErros + " lexical errors and "
        			+ parser.getNumberOfSyntaxErrors() + " syntax errors");
        
    }
}
