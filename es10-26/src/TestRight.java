import java.io.*;
import org.antlr.v4.runtime.*;

//SimpleExpLexer
//SimpleExpParser
//CommonTokenStream
//lexicalErrors
//getNumberOfSyntaxErrors

public class TestRight {
    public static void main(String[] args) throws Exception {

        String fileName = "prova.txt";
     
        CharStream chars = CharStreams.fromFileName(fileName);  //dal file creiamo uno stream di caratteri che poi daremo in pasto 
        SimpleExpRightLexer lexer = new SimpleExpRightLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SimpleExpRightParser parser = new SimpleExpRightParser(tokens);
        parser.prog(); //la variabile � diventata un metodo
        
        System.out.println("Right. You had: " + lexer.lexicalErros + " lexical errors and "
        			+ parser.getNumberOfSyntaxErrors() + " syntax errors");
        
    }
}
