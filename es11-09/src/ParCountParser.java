// Generated from ParCount.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ParCountParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LPAR=1, RPAR=2, LSPAR=3, RSPAR=4, NUM=5, WHITESP=6, COMMENT=7, ERR=8;
	public static final int
		RULE_prog = 0, RULE_exp = 1;
	public static final String[] ruleNames = {
		"prog", "exp"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'['", "']'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "LPAR", "RPAR", "LSPAR", "RSPAR", "NUM", "WHITESP", "COMMENT", "ERR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "ParCount.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ParCountParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public ExpContext e;
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(4);
			((ProgContext)_localctx).e = exp();
			System.out.println("Translated value: " + ((ProgContext)_localctx).e.trans);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public int trans;
		public ExpContext e;
		public TerminalNode LPAR() { return getToken(ParCountParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(ParCountParser.RPAR, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public TerminalNode LSPAR() { return getToken(ParCountParser.LSPAR, 0); }
		public TerminalNode RSPAR() { return getToken(ParCountParser.RSPAR, 0); }
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
	}

	public final ExpContext exp() throws RecognitionException {
		ExpContext _localctx = new ExpContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_exp);
		try {
			setState(18);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EOF:
			case RPAR:
			case RSPAR:
				enterOuterAlt(_localctx, 1);
				{
				((ExpContext)_localctx).trans = 0;
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(8);
				match(LPAR);
				setState(9);
				((ExpContext)_localctx).e = exp();
				((ExpContext)_localctx).trans = ((ExpContext)_localctx).e.trans+1;
				setState(11);
				match(RPAR);
				}
				break;
			case LSPAR:
				enterOuterAlt(_localctx, 3);
				{
				setState(13);
				match(LSPAR);
				setState(14);
				((ExpContext)_localctx).e = exp();
				setState(15);
				match(RSPAR);
				((ExpContext)_localctx).trans = ((ExpContext)_localctx).e.trans;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\n\27\4\2\t\2\4\3"+
		"\t\3\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\25\n"+
		"\3\3\3\2\2\4\2\4\2\2\2\26\2\6\3\2\2\2\4\24\3\2\2\2\6\7\5\4\3\2\7\b\b\2"+
		"\1\2\b\3\3\2\2\2\t\25\b\3\1\2\n\13\7\3\2\2\13\f\5\4\3\2\f\r\b\3\1\2\r"+
		"\16\7\4\2\2\16\25\3\2\2\2\17\20\7\5\2\2\20\21\5\4\3\2\21\22\7\6\2\2\22"+
		"\23\b\3\1\2\23\25\3\2\2\2\24\t\3\2\2\2\24\n\3\2\2\2\24\17\3\2\2\2\25\5"+
		"\3\2\2\2\3\24";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}