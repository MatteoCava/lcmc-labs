import java.io.*;
import org.antlr.v4.runtime.*;

public class TestRight {
    public static void main(String[] args) throws Exception {

        String fileName = "prova.txt";
     
        CharStream chars = CharStreams.fromFileName(fileName);
        SimpleExpRightLexer lexer = new SimpleExpRightLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SimpleExpRightParser parser = new SimpleExpRightParser(tokens);
        
        parser.prog();
        
        System.out.println(
        		"Right. You had: "+lexer.lexicalErrors+" lexical errors and "
        		+parser.getNumberOfSyntaxErrors()+" syntax errors."
        		);
        
    }
}
