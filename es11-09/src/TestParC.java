import java.io.*;
import org.antlr.v4.runtime.*;

public class TestParC {
    public static void main(String[] args) throws Exception {

        String fileName = "provaparc.txt";
     
        CharStream chars = CharStreams.fromFileName(fileName);
        ParCountLexer lexer = new ParCountLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ParCountParser parser = new ParCountParser(tokens);
        
        parser.prog();
        
        System.out.println(
        		"ParCount. You had: "+lexer.lexicalErrors+" lexical errors and "
        		+parser.getNumberOfSyntaxErrors()+" syntax errors."
        		);
        
    }
}
