grammar SimpleExp;

@lexer::members {
int lexicalErrors=0;
}

// PARSER RULES

//VERISONE ASSOCIATIVA A SX

prog : e=exp {System.out.println("Translated value: " + $e.trans);} ;

/*con * ho un ciclo in cui posso incontrare tanti altri plus term,  
ogni volta che incontro un +, sommo quello che ho incotntrato con quello contenuto dentro tras*/
exp returns [int trans] : t=term {$trans=$t.trans;} 
							(PLUS t=term {$trans+=$t.trans;})* ; 

term returns [int trans] : v=value {$trans=$v.trans;} 
							(TIMES v=value {$trans*=$v.trans;})* ;

value returns [int trans] : n=NUM {$trans=Integer.parseInt($n.text);}  
							| LPAR e=exp RPAR {$trans=$e.trans;};

// LEXER RULES
	
PLUS	: '+' ; //se metto minus non ho problemi a differenza del SimpleExpRight
TIMES	: '*' ;
LPAR    : '(' ;
RPAR    : ')' ;
NUM     : ('1'..'9')('0'..'9')* | '0' ;

WHITESP	: (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;
COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR 	: . {System.out.println("Invalid char: "+ getText()); lexicalErrors++;} -> channel(HIDDEN) ; 

