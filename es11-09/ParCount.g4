grammar ParCount;

@lexer::members {
int lexicalErrors=0;
}
  
// PARSER RULES
 
prog : e=exp {System.out.println("Translated value: " + $e.trans);} ;

 //trans � una variabile che possiamo usare per fargli ritrnare qualcosa
exp returns [int trans]: /* epsilon */ {$trans=0;} //corrisponde a dire che quello che ritorna la testa � 0
    | LPAR e=exp {$trans=$e.trans+1;} RPAR  //exp sar� come una chiamata ricorsiva che alla fine ritorna un qualcosa con propriet� trans
	| LSPAR e=exp RSPAR {$trans=$e.trans;}; 
	
	// trans � come una variabile locale di tipo int
            
// LEXER RULES
	
LPAR	: '(' ;
RPAR	: ')' ;
LSPAR	: '[' ;
RSPAR	: ']' ;
NUM	: ('1'..'9')('0'..'9')* | '0' ;   

WHITESP : (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;
COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR	    : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 