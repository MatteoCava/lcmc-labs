grammar SimpleExpRight;

@lexer::members {
int lexicalErrors=0;
}
 
// PARSER RULES

prog : e=exp {System.out.println("Translated value: " + $e.trans);} ;

//exp pu� essere o term da solo o term plus exp
// la somma devo farla solo se viene fatto il punto interrogativo
exp returns [int trans] : t=term {$trans=$t.trans;} (PLUS e=exp {$trans+=$e.trans;})?;  // exp pu� essere o temp da solo o term+exp 

term returns [int trans] : v=value {$trans=$v.trans;} (TIMES t=term {$trans*=$t.trans;})? ;
//num � un token, in text mi viene dato il lessema che ha matchato con num
value returns [int trans] : n=NUM {$trans=Integer.parseInt($n.text);} 
							| LPAR e=exp {$trans=$e.trans;} RPAR ;

// LEXER RULES
	
PLUS	: '+' ; //se avessi avuto il meno non funzionava correttametne visto che raggruppa a dx: con 6-3-2 avrebbe fatto prima 3-2 e poi 6-1
TIMES	: '*' ;
LPAR    : '(' ;
RPAR    : ')' ; 
NUM     : ('1'..'9')('0'..'9')* | '0' ;

WHITESP	: (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;
COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR 	: . {System.out.println("Invalid char: "+ getText()); lexicalErrors++;} -> channel(HIDDEN); 

