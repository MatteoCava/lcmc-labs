// Generated from FOOL.g4 by ANTLR 4.7

	import ast.*;
	import java.util.ArrayList;
	import java.util.HashMap;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FOOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SEMIC=1, COLON=2, COMMA=3, EQ=4, ASS=5, PLUS=6, TIMES=7, INTEGER=8, TRUE=9, 
		FALSE=10, LPAR=11, RPAR=12, CLPAR=13, CRPAR=14, IF=15, THEN=16, ELSE=17, 
		PRINT=18, LET=19, IN=20, VAR=21, FUN=22, INT=23, BOOL=24, ID=25, WHITESP=26, 
		COMMENT=27, ERR=28;
	public static final int
		RULE_prog = 0, RULE_declist = 1, RULE_type = 2, RULE_exp = 3, RULE_term = 4, 
		RULE_factor = 5, RULE_value = 6;
	public static final String[] ruleNames = {
		"prog", "declist", "type", "exp", "term", "factor", "value"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "':'", "','", "'=='", "'='", "'+'", "'*'", null, "'true'", 
		"'false'", "'('", "')'", "'{'", "'}'", "'if'", "'then'", "'else'", "'print'", 
		"'let'", "'in'", "'var'", "'fun'", "'int'", "'bool'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "SEMIC", "COLON", "COMMA", "EQ", "ASS", "PLUS", "TIMES", "INTEGER", 
		"TRUE", "FALSE", "LPAR", "RPAR", "CLPAR", "CRPAR", "IF", "THEN", "ELSE", 
		"PRINT", "LET", "IN", "VAR", "FUN", "INT", "BOOL", "ID", "WHITESP", "COMMENT", 
		"ERR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "FOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	 //diventano variabili della classe parser che va a creare
		
		
		//variabile globale che contiene il nesting level corrente, quando entriamo in uno scope la incrementiamo, quando usciamo la decrementiamo
		
		private int nestingLevel = 0; 
		
		//lista di mappe stringa-id a stentry che contiene info sulla dichiarazione 
		//lista di mappe perch� ne abbiamo una per livello fino ad arrivare a quello globale
		private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
		//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
		//il "fronte" della lista di tabelle � symTable.get(nestingLevel)

	public FOOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public Node ast;
		public ExpContext e;
		public DeclistContext d;
		public TerminalNode SEMIC() { return getToken(FOOLParser.SEMIC, 0); }
		public TerminalNode LET() { return getToken(FOOLParser.LET, 0); }
		public TerminalNode IN() { return getToken(FOOLParser.IN, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public DeclistContext declist() {
			return getRuleContext(DeclistContext.class,0);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		try {
			enterOuterAlt(_localctx, 1);
			{

						HashMap<String, STentry> hm = new HashMap<String, STentry>();
						symTable.add(hm); //mi creo una tabella da usare per il livello 0
					
			setState(24);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
			case TRUE:
			case FALSE:
			case LPAR:
			case IF:
			case PRINT:
			case ID:
				{
				setState(15);
				((ProgContext)_localctx).e = exp();
				((ProgContext)_localctx).ast =  new ProgNode(((ProgContext)_localctx).e.ast);
				}
				break;
			case LET:
				{
				setState(18);
				match(LET);
				setState(19);
				((ProgContext)_localctx).d = declist();
				setState(20);
				match(IN);
				setState(21);
				((ProgContext)_localctx).e = exp();
				((ProgContext)_localctx).ast =  new ProgLetInNode(((ProgContext)_localctx).d.astlist, ((ProgContext)_localctx).e.ast);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			 symTable.remove(0);
			setState(27);
			match(SEMIC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclistContext extends ParserRuleContext {
		public ArrayList<Node> astlist;
		public Token i;
		public TypeContext t;
		public ExpContext e;
		public DeclistContext d;
		public List<TerminalNode> SEMIC() { return getTokens(FOOLParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(FOOLParser.SEMIC, i);
		}
		public List<TerminalNode> VAR() { return getTokens(FOOLParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(FOOLParser.VAR, i);
		}
		public List<TerminalNode> COLON() { return getTokens(FOOLParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(FOOLParser.COLON, i);
		}
		public List<TerminalNode> ASS() { return getTokens(FOOLParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(FOOLParser.ASS, i);
		}
		public List<TerminalNode> FUN() { return getTokens(FOOLParser.FUN); }
		public TerminalNode FUN(int i) {
			return getToken(FOOLParser.FUN, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(FOOLParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(FOOLParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(FOOLParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(FOOLParser.RPAR, i);
		}
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> LET() { return getTokens(FOOLParser.LET); }
		public TerminalNode LET(int i) {
			return getToken(FOOLParser.LET, i);
		}
		public List<TerminalNode> IN() { return getTokens(FOOLParser.IN); }
		public TerminalNode IN(int i) {
			return getToken(FOOLParser.IN, i);
		}
		public List<DeclistContext> declist() {
			return getRuleContexts(DeclistContext.class);
		}
		public DeclistContext declist(int i) {
			return getRuleContext(DeclistContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public DeclistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declist; }
	}

	public final DeclistContext declist() throws RecognitionException {
		DeclistContext _localctx = new DeclistContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			 ((DeclistContext)_localctx).astlist =  new ArrayList<Node>();
			setState(76); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(72);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case VAR:
					{
					setState(30);
					match(VAR);
					setState(31);
					((DeclistContext)_localctx).i = match(ID);
					setState(32);
					match(COLON);
					setState(33);
					((DeclistContext)_localctx).t = type();
					setState(34);
					match(ASS);
					setState(35);
					((DeclistContext)_localctx).e = exp();

						            		VarNode v = new VarNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), ((DeclistContext)_localctx).t.ast, ((DeclistContext)_localctx).e.ast); //ovviamente rappresenta la dichiarazione di variabili
						            		_localctx.astlist.add(v); //aggiungo alla declist
						            		HashMap<String, STentry> hm = symTable.get(nestingLevel); //tabella nel fronte della lista
						            		//devo guardare se c'� e nel caso sostituirla, altrimenti aggiungerla
						            		
						            		//non controllo perch� il put di java se c'� gi� ritorna il valore vecchio (quindi lo mette e mi dice se c'era gi�)
						            		if (hm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), new STentry(nestingLevel)) != null) {
						            			//line � il numero di linea del sorgente dove ha matchato, sono informazioni che il lexer raccoglie
						            			System.out.println("Var id "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null)+" at line "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getLine():0)+" already declared"); //se c'� gi� allora errore!
												System.exit(0);
						            		}
						            	
					}
					break;
				case FUN:
					{
					setState(38);
					match(FUN);
					setState(39);
					((DeclistContext)_localctx).i = match(ID);
					setState(40);
					match(COLON);
					setState(41);
					((DeclistContext)_localctx).t = type();

						              		//nel funnode non devo aggiungere subito tutti i parametri ma posso farlo man mano
						              		FunNode f = new FunNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), ((DeclistContext)_localctx).t.ast);
						              		_localctx.astlist.add(f);
						              		HashMap<String, STentry> hm = symTable.get(nestingLevel); //STESSA COSA DELLA DICHIARAZIONE DI VARIABILE MA APPLICATA ALLE FUNZIONI
						            		//devo guardare se c'� e nel caso sostituirla, altrimenti aggiungerla
						            		if (hm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), new STentry(nestingLevel)) != null) {//non controllo perch� il put di java se c'� gi� ritorna il valore vecchio (quindi lo mette e mi dice se c'era gi�)
						            			System.out.println("Var id "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null)+" at line "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getLine():0)+" already declared");
												System.exit(0);
						            		}
						              		//ENTRIAMO IN UN NUOVO SCOPE, prima della dichiarazione dei parametri <<<<<<<<<<-----------------
						              		nestingLevel++;
						              		//creo una nuova hasmap da usare per il nuovo livello
						              		HashMap<String, STentry> nhm = new HashMap<String, STentry>(); //nhm sta per newhashmap
						              		symTable.add(nhm);
						              	
					setState(43);
					match(LPAR);
					setState(59);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==ID) {
						{
						setState(44);
						((DeclistContext)_localctx).i = match(ID);
						setState(45);
						match(COLON);
						setState(46);
						((DeclistContext)_localctx).t = type();
						 
							              				ParNode parNode = new ParNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), ((DeclistContext)_localctx).t.ast);
							              				f.addPar(parNode);
							              				nhm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), new STentry(nestingLevel));
							              			
						setState(56);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(48);
							match(COMMA);
							setState(49);
							((DeclistContext)_localctx).i = match(ID);
							setState(50);
							match(COLON);
							setState(51);
							((DeclistContext)_localctx).t = type();

									              				ParNode parNode2 = new ParNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), ((DeclistContext)_localctx).t.ast);
									              				f.addPar(parNode2);
									              				if (hm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), new STentry(nestingLevel)) != null) {//non controllo perch� il put di java se c'� gi� ritorna il valore vecchio (quindi lo mette e mi dice se c'era gi�)
											            			System.out.pri ntln("Var id "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null)+" at line "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getLine():0)+" already declared");
																	System.exit(0);
								            					}
								              				
							}
							}
							setState(58);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(61);
					match(RPAR);
					setState(67);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LET) {
						{
						setState(62);
						match(LET);
						setState(63);
						((DeclistContext)_localctx).d = declist();
						setState(64);
						match(IN);
						 f.addDec(((DeclistContext)_localctx).d.astlist); 
						}
					}

					setState(69);
					((DeclistContext)_localctx).e = exp();

						              		f.addBody(((DeclistContext)_localctx).e.ast);
						              		//esco dallo scope   <<<<<<<<<<<<<<<-----------------------------------
						              		symTable.remove(nestingLevel--); //post decremento (come fare la remove e poi decrementare)
						              	
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(74);
				match(SEMIC);
				}
				}
				setState(78); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==VAR || _la==FUN );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Node ast;
		public TerminalNode INT() { return getToken(FOOLParser.INT, 0); }
		public TerminalNode BOOL() { return getToken(FOOLParser.BOOL, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_type);
		try {
			setState(84);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(80);
				match(INT);
				((TypeContext)_localctx).ast =  new IntTypeNode();
				}
				break;
			case BOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(82);
				match(BOOL);
				((TypeContext)_localctx).ast =  new BoolTypeNode();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public Node ast;
		public TermContext f;
		public TermContext l;
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<TerminalNode> PLUS() { return getTokens(FOOLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(FOOLParser.PLUS, i);
		}
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
	}

	public final ExpContext exp() throws RecognitionException {
		ExpContext _localctx = new ExpContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			((ExpContext)_localctx).f = term();
			((ExpContext)_localctx).ast =  ((ExpContext)_localctx).f.ast;
			setState(94);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PLUS) {
				{
				{
				setState(88);
				match(PLUS);
				setState(89);
				((ExpContext)_localctx).l = term();
				((ExpContext)_localctx).ast =  new PlusNode (_localctx.ast,((ExpContext)_localctx).l.ast);
				}
				}
				setState(96);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public Node ast;
		public FactorContext f;
		public FactorContext l;
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public List<TerminalNode> TIMES() { return getTokens(FOOLParser.TIMES); }
		public TerminalNode TIMES(int i) {
			return getToken(FOOLParser.TIMES, i);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			((TermContext)_localctx).f = factor();
			((TermContext)_localctx).ast =  ((TermContext)_localctx).f.ast;
			setState(105);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TIMES) {
				{
				{
				setState(99);
				match(TIMES);
				setState(100);
				((TermContext)_localctx).l = factor();
				((TermContext)_localctx).ast =  new TimesNode (_localctx.ast,((TermContext)_localctx).l.ast);
				}
				}
				setState(107);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public Node ast;
		public ValueContext f;
		public ValueContext l;
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public List<TerminalNode> EQ() { return getTokens(FOOLParser.EQ); }
		public TerminalNode EQ(int i) {
			return getToken(FOOLParser.EQ, i);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_factor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(108);
			((FactorContext)_localctx).f = value();
			((FactorContext)_localctx).ast =  ((FactorContext)_localctx).f.ast;
			setState(116);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==EQ) {
				{
				{
				setState(110);
				match(EQ);
				setState(111);
				((FactorContext)_localctx).l = value();
				((FactorContext)_localctx).ast =  new EqualNode (_localctx.ast,((FactorContext)_localctx).l.ast);
				}
				}
				setState(118);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public Node ast;
		public Token n;
		public ExpContext e;
		public ExpContext x;
		public ExpContext y;
		public ExpContext z;
		public Token i;
		public TerminalNode INTEGER() { return getToken(FOOLParser.INTEGER, 0); }
		public TerminalNode TRUE() { return getToken(FOOLParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(FOOLParser.FALSE, 0); }
		public TerminalNode LPAR() { return getToken(FOOLParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(FOOLParser.RPAR, 0); }
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode IF() { return getToken(FOOLParser.IF, 0); }
		public TerminalNode THEN() { return getToken(FOOLParser.THEN, 0); }
		public List<TerminalNode> CLPAR() { return getTokens(FOOLParser.CLPAR); }
		public TerminalNode CLPAR(int i) {
			return getToken(FOOLParser.CLPAR, i);
		}
		public List<TerminalNode> CRPAR() { return getTokens(FOOLParser.CRPAR); }
		public TerminalNode CRPAR(int i) {
			return getToken(FOOLParser.CRPAR, i);
		}
		public TerminalNode ELSE() { return getToken(FOOLParser.ELSE, 0); }
		public TerminalNode PRINT() { return getToken(FOOLParser.PRINT, 0); }
		public TerminalNode ID() { return getToken(FOOLParser.ID, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_value);
		try {
			setState(150);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
				enterOuterAlt(_localctx, 1);
				{
				setState(119);
				((ValueContext)_localctx).n = match(INTEGER);
				((ValueContext)_localctx).ast =  new IntNode(Integer.parseInt((((ValueContext)_localctx).n!=null?((ValueContext)_localctx).n.getText():null)));
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 2);
				{
				setState(121);
				match(TRUE);
				((ValueContext)_localctx).ast =  new BoolNode(true);
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 3);
				{
				setState(123);
				match(FALSE);
				((ValueContext)_localctx).ast =  new BoolNode(false);
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 4);
				{
				setState(125);
				match(LPAR);
				setState(126);
				((ValueContext)_localctx).e = exp();
				setState(127);
				match(RPAR);
				((ValueContext)_localctx).ast =  ((ValueContext)_localctx).e.ast;
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 5);
				{
				setState(130);
				match(IF);
				setState(131);
				((ValueContext)_localctx).x = exp();
				setState(132);
				match(THEN);
				setState(133);
				match(CLPAR);
				setState(134);
				((ValueContext)_localctx).y = exp();
				setState(135);
				match(CRPAR);
				setState(136);
				match(ELSE);
				setState(137);
				match(CLPAR);
				setState(138);
				((ValueContext)_localctx).z = exp();
				setState(139);
				match(CRPAR);
				((ValueContext)_localctx).ast =  new IfNode(((ValueContext)_localctx).x.ast,((ValueContext)_localctx).y.ast,((ValueContext)_localctx).z.ast);
				}
				break;
			case PRINT:
				enterOuterAlt(_localctx, 6);
				{
				setState(142);
				match(PRINT);
				setState(143);
				match(LPAR);
				setState(144);
				((ValueContext)_localctx).e = exp();
				setState(145);
				match(RPAR);
				((ValueContext)_localctx).ast =  new PrintNode(((ValueContext)_localctx).e.ast);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 7);
				{
				setState(148);
				((ValueContext)_localctx).i = match(ID);
				 //cerco la dichiarazione nella tabella <<<<<<<<<<<-------------------
							int j = nestingLevel;
							STentry entry = null; //quando trovo la pallina facendo la ricerca la metto qua
							while (j >= 0 && entry == null) { //cerco l'id nella lista di tabella andando gi� di nesting level
								entry = symTable.get(j--).get((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null));
							
							} 
							if (entry == null) {
								System.out.println("id "+(((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null)+" at line "+(((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getLine():0)+" not declared");
								System.exit(0);
							}
							((ValueContext)_localctx).ast =  new IdNode((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null), entry);
						
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\36\u009b\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\5\2\33\n\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\7\39\n\3\f\3\16\3<\13\3\5\3>\n\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3F\n\3\3"+
		"\3\3\3\3\3\5\3K\n\3\3\3\3\3\6\3O\n\3\r\3\16\3P\3\4\3\4\3\4\3\4\5\4W\n"+
		"\4\3\5\3\5\3\5\3\5\3\5\3\5\7\5_\n\5\f\5\16\5b\13\5\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\7\6j\n\6\f\6\16\6m\13\6\3\7\3\7\3\7\3\7\3\7\3\7\7\7u\n\7\f\7\16\7"+
		"x\13\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u0099"+
		"\n\b\3\b\2\2\t\2\4\6\b\n\f\16\2\2\2\u00a3\2\20\3\2\2\2\4\37\3\2\2\2\6"+
		"V\3\2\2\2\bX\3\2\2\2\nc\3\2\2\2\fn\3\2\2\2\16\u0098\3\2\2\2\20\32\b\2"+
		"\1\2\21\22\5\b\5\2\22\23\b\2\1\2\23\33\3\2\2\2\24\25\7\25\2\2\25\26\5"+
		"\4\3\2\26\27\7\26\2\2\27\30\5\b\5\2\30\31\b\2\1\2\31\33\3\2\2\2\32\21"+
		"\3\2\2\2\32\24\3\2\2\2\33\34\3\2\2\2\34\35\b\2\1\2\35\36\7\3\2\2\36\3"+
		"\3\2\2\2\37N\b\3\1\2 !\7\27\2\2!\"\7\33\2\2\"#\7\4\2\2#$\5\6\4\2$%\7\7"+
		"\2\2%&\5\b\5\2&\'\b\3\1\2\'K\3\2\2\2()\7\30\2\2)*\7\33\2\2*+\7\4\2\2+"+
		",\5\6\4\2,-\b\3\1\2-=\7\r\2\2./\7\33\2\2/\60\7\4\2\2\60\61\5\6\4\2\61"+
		":\b\3\1\2\62\63\7\5\2\2\63\64\7\33\2\2\64\65\7\4\2\2\65\66\5\6\4\2\66"+
		"\67\b\3\1\2\679\3\2\2\28\62\3\2\2\29<\3\2\2\2:8\3\2\2\2:;\3\2\2\2;>\3"+
		"\2\2\2<:\3\2\2\2=.\3\2\2\2=>\3\2\2\2>?\3\2\2\2?E\7\16\2\2@A\7\25\2\2A"+
		"B\5\4\3\2BC\7\26\2\2CD\b\3\1\2DF\3\2\2\2E@\3\2\2\2EF\3\2\2\2FG\3\2\2\2"+
		"GH\5\b\5\2HI\b\3\1\2IK\3\2\2\2J \3\2\2\2J(\3\2\2\2KL\3\2\2\2LM\7\3\2\2"+
		"MO\3\2\2\2NJ\3\2\2\2OP\3\2\2\2PN\3\2\2\2PQ\3\2\2\2Q\5\3\2\2\2RS\7\31\2"+
		"\2SW\b\4\1\2TU\7\32\2\2UW\b\4\1\2VR\3\2\2\2VT\3\2\2\2W\7\3\2\2\2XY\5\n"+
		"\6\2Y`\b\5\1\2Z[\7\b\2\2[\\\5\n\6\2\\]\b\5\1\2]_\3\2\2\2^Z\3\2\2\2_b\3"+
		"\2\2\2`^\3\2\2\2`a\3\2\2\2a\t\3\2\2\2b`\3\2\2\2cd\5\f\7\2dk\b\6\1\2ef"+
		"\7\t\2\2fg\5\f\7\2gh\b\6\1\2hj\3\2\2\2ie\3\2\2\2jm\3\2\2\2ki\3\2\2\2k"+
		"l\3\2\2\2l\13\3\2\2\2mk\3\2\2\2no\5\16\b\2ov\b\7\1\2pq\7\6\2\2qr\5\16"+
		"\b\2rs\b\7\1\2su\3\2\2\2tp\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2w\r\3"+
		"\2\2\2xv\3\2\2\2yz\7\n\2\2z\u0099\b\b\1\2{|\7\13\2\2|\u0099\b\b\1\2}~"+
		"\7\f\2\2~\u0099\b\b\1\2\177\u0080\7\r\2\2\u0080\u0081\5\b\5\2\u0081\u0082"+
		"\7\16\2\2\u0082\u0083\b\b\1\2\u0083\u0099\3\2\2\2\u0084\u0085\7\21\2\2"+
		"\u0085\u0086\5\b\5\2\u0086\u0087\7\22\2\2\u0087\u0088\7\17\2\2\u0088\u0089"+
		"\5\b\5\2\u0089\u008a\7\20\2\2\u008a\u008b\7\23\2\2\u008b\u008c\7\17\2"+
		"\2\u008c\u008d\5\b\5\2\u008d\u008e\7\20\2\2\u008e\u008f\b\b\1\2\u008f"+
		"\u0099\3\2\2\2\u0090\u0091\7\24\2\2\u0091\u0092\7\r\2\2\u0092\u0093\5"+
		"\b\5\2\u0093\u0094\7\16\2\2\u0094\u0095\b\b\1\2\u0095\u0099\3\2\2\2\u0096"+
		"\u0097\7\33\2\2\u0097\u0099\b\b\1\2\u0098y\3\2\2\2\u0098{\3\2\2\2\u0098"+
		"}\3\2\2\2\u0098\177\3\2\2\2\u0098\u0084\3\2\2\2\u0098\u0090\3\2\2\2\u0098"+
		"\u0096\3\2\2\2\u0099\17\3\2\2\2\r\32:=EJPV`kv\u0098";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}