package ast;

import java.util.ArrayList;

public class FunNode implements Node {

	private String id; // nome della funzione
	private Node type; // tipo di ritorno
	private Node exp; //corpo della funzione
	//parlist
	private ArrayList<Node> parlist = new ArrayList<Node>();
	private ArrayList<Node> declist = new ArrayList<Node>(); // lista delle dichiarazioni, nel caso in cui non ci sia la
																// let in sarebbe null, quindi meglio inizializarloo
	// per esercizion la lista dei paramentri

	public FunNode(String id, Node type) {
		this.id = id;
		this.type = type;
	}

	//aggiunta lista delle dichiarazioni
	public void addDec(ArrayList<Node> d) {
		this.declist = d;
	}

	//aggiunta corpo funzione
	public void addBody(Node e) {
		this.exp = e;
	}
	
	//addPar che aggiunge alla parlist un nodo ParNode(dichiarazine di un parametro)
	public void addPar(ParNode node) {
		this.parlist.add(node);
	}

	public String toPrint(String s) {
		String declstr= "";
		String parstr = "";
		//liste con le toprint di tutti i nodi delle liste
		for (Node dec: declist) declstr += dec.toPrint(s+ " ");
		for (Node par: parlist) parstr += par.toPrint(s+ " ");
		return s + "Fun: " + id + " \n" 
				+ type.toPrint(s + " ")
				+ parstr
				+ declstr
				+ exp.toPrint(s + " ");
	}

}