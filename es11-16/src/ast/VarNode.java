package ast;

//mi rappresenta la dichirazione di variabili
public class VarNode implements Node {

	private String id;
	private Node type;
	private Node exp;

	public VarNode(String id, Node type, Node exp) {
		this.id = id;
		this.type = type;
		this.exp = exp;
	}

	public String toPrint(String s) {
		return s + "Var: " + id + " \n" + type.toPrint(s + "  ") + exp.toPrint(s + "  ");
	}

}