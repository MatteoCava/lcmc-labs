package ast;

//viene arricchitto con la "pallina dell'albero"
public class IdNode implements Node {

	private String id;
	private STentry entry;

	public IdNode(String id, STentry entry) {
		this.id = id;
		this.entry = entry;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + "\n" + entry.toPrint(s + "  ");
	}

}