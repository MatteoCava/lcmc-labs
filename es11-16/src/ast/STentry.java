package ast;

//questa classe si arricchir� sempre pi� di informazioni
public class STentry {

	private int nestingLevel;

	public STentry(int nestingLevel) {
		this.nestingLevel = nestingLevel;
	}

	public String toPrint(String s) {
		return s + "STentry: nesting level " + nestingLevel + "\n";
	}

}
