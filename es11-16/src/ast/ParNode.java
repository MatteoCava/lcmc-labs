package ast;

//nodo parametro della funzione
public class ParNode implements Node {

	private String id;
	private Node type;

	public ParNode(String id, Node type) {
		this.id = id;
		this.type = type;
	}

	public String toPrint(String s) {
		return s + "Par: " + id + " \n" + type.toPrint(s + "  ");
	}

}