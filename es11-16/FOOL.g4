grammar FOOL;

@header {
	import ast.*;
	import java.util.ArrayList;
	import java.util.HashMap;
}

@parser::members { //diventano variabili della classe parser che va a creare
	
	
	//variabile globale che contiene il nesting level corrente, quando entriamo in uno scope la incrementiamo, quando usciamo la decrementiamo
	
	private int nestingLevel = 0; 
	
	//lista di mappe stringa-id a stentry che contiene info sulla dichiarazione 
	//lista di mappe perch� ne abbiamo una per livello fino ad arrivare a quello globale
	private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
	//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle � symTable.get(nestingLevel)
}

@lexer::members {
	int lexicalErrors=0;
}


/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
 
 /* il programma o � semplicemnte un'espressione seguita dal ; o ho anche il let in
  * se ho il let in ho una nuova variabile declist che � una LISTA DI DICHIARAZIONI
  */

prog	returns [Node ast]
		: {
			HashMap<String, STentry> hm = new HashMap<String, STentry>();
			symTable.add(hm); //mi creo una tabella da usare per il livello 0
		}
		( e=exp	
         	{$ast = new ProgNode($e.ast);}
         	| LET d=declist IN e=exp
        	{$ast = new ProgLetInNode($d.astlist, $e.ast);}
           
        ) 
        { symTable.remove(0);} //posso anche passare nesting level come parametro, tanto � 0 - sono alla fine del parsing! quando esco dal livello non serve pi�
        SEMIC;

// declist = lista di dichiarazioni
declist returns [ArrayList<Node> astlist] : { $astlist = new ArrayList<Node>();}  //creazione lista vuota per poi poterli inserire pi� in basso
		( //� una sequenza di una o pi� dichiarazioni, ognuna di esse � o comincia con var o con fun e alla fine in ogni caso finisce con semicol
            (   VAR i=ID COLON t=type ASS e=exp // ASS assegnamento ovvero =
	            	{
	            		VarNode v = new VarNode($i.text, $t.ast, $e.ast); //ovviamente rappresenta la dichiarazione di variabili
	            		$astlist.add(v); //aggiungo alla declist
	            		HashMap<String, STentry> hm = symTable.get(nestingLevel); //tabella nel fronte della lista
	            		//devo guardare se c'� e nel caso sostituirla, altrimenti aggiungerla
	            		
	            		//non controllo perch� il put di java se c'� gi� ritorna il valore vecchio (quindi lo mette e mi dice se c'era gi�)
	            		if (hm.put($i.text, new STentry(nestingLevel)) != null) {
	            			//line � il numero di linea del sorgente dove ha matchato, sono informazioni che il lexer raccoglie
	            			System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared"); //se c'� gi� allora errore!
							System.exit(0);
	            		}
	            	}
	            	
	           // la parte della fun al completo �:  il punto interrogrativo mi dice che quello tra le parentesi tonde della fun � opzionale(possono non esserci parametri)
	          // anche let in nella funzione � opzionale perch� potrebbe non avere dichiarazioni di var
	          // NON c'� il return, il programma � un'espressione che ritorna un valore e la funzione � un qualcosa che ha un corpo con un'espressine che quando ritorna mi da il risultato della fun
	            // FUN ID COLON type LPAR (ID COLON type (COMMA ID COLON type)* )? RPAR (LET declist IN)? exp 
	            	/*? mi dice che che tra le parentesi potrebbe non esserci nulla, dentro poi posso avere o un parametro solo o tanti 
	            	 * (con la , che ci deve essere solo quando ho pi� di un parametro) 
	            	* nella parte let in opzionale riuso la variabile declicst*/ 
	            	| FUN i=ID COLON t=type 
	              	{
	              		//nel funnode non devo aggiungere subito tutti i parametri ma posso farlo man mano
	              		FunNode f = new FunNode($i.text, $t.ast);
	              		$astlist.add(f);
	              		HashMap<String, STentry> hm = symTable.get(nestingLevel); //STESSA COSA DELLA DICHIARAZIONE DI VARIABILE MA APPLICATA ALLE FUNZIONI
	            		//devo guardare se c'� e nel caso sostituirla, altrimenti aggiungerla
	            		if (hm.put($i.text, new STentry(nestingLevel)) != null) {//non controllo perch� il put di java se c'� gi� ritorna il valore vecchio (quindi lo mette e mi dice se c'era gi�)
	            			System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
							System.exit(0);
	            		}
	              		//ENTRIAMO IN UN NUOVO SCOPE, prima della dichiarazione dei parametri <<<<<<<<<<-----------------
	              		nestingLevel++;
	              		//creo una nuova hasmap da usare per il nuovo livello
	              		HashMap<String, STentry> nhm = new HashMap<String, STentry>(); //nhm sta per newhashmap
	              		symTable.add(nhm);
	              	}

	              	//da qui cominciano i parametri
	              	/*quando incotnriamo una dichiarazione di un parametro � come una dichairazione di una variabile
	              	 * ma senza exp. bisogna quindi creare il ParNode (classe per la dichairazione di paramentri praticamente uguale al VarNode
	              	 * ma senza exp) mettendolo come 
	              	 * in FunNOde avremo una funzione addPAr e quini una var parlist
	              	 */
	              	 //campo "parlist" che e' una lista di Node (inizializzato ad una lista vuota)
					//metodo "addPar()" che aggiunge un nodo al campo parList
	              	LPAR //DICHIARAZIONE DEI PARAMENTRI
	              		(i=ID COLON t=type//primo parametro dichiarato
	              			
	              			//creero oggetto ParNode e lo aggiungo al fun node col metodo che sar� da fare
	              			{ 
	              				ParNode parNode = new ParNode($i.text, $t.ast);
	              				f.addPar(parNode);
	              				nhm.put($i.text, new STentry(nestingLevel));
	              			} 
	              			//creare il ParNode
							//lo attacco al FunNode invocando addPar 
							//aggiungo una STentry alla hashmap nhm
	              			(COMMA i=ID COLON t=type//parametri successivi 
	              				{
		              				ParNode parNode2 = new ParNode($i.text, $t.ast);
		              				f.addPar(parNode2);
		              				if (hm.put($i.text, new STentry(nestingLevel)) != null) {//non controllo perch� il put di java se c'� gi� ritorna il valore vecchio (quindi lo mette e mi dice se c'era gi�)
				            			System.out.pri ntln("Var id "+$i.text+" at line "+$i.line+" already declared");
										System.exit(0);
	            					}
	              				}
	              				//parametri successivi
	              				//idem ma devo controllare che non ci sia un indentificatore uguale dopo il primo
	              			)*
	              		)?
	              		 
	              	RPAR 
	              	
	              	
	              	
	              	(LET d=declist IN { f.addDec($d.astlist); } )? e=exp //add dec se ci sono delle dichiarazioni, altrimenti non c'� nulla
	              	{
	              		f.addBody($e.ast);
	              		//esco dallo scope   <<<<<<<<<<<<<<<-----------------------------------
	              		symTable.remove(nestingLevel--); //post decremento (come fare la remove e poi decrementare)
	              	} 
	
			) SEMIC 
		)+ //chiusura positiva, ripete una o pi� volte
		;
        
type returns [Node ast] 
	: INT 	{$ast = new IntTypeNode();}   	 	      
    | BOOL 	{$ast = new BoolTypeNode();}		      	
 	;   
	 	 
exp	returns [Node ast]
 	: f=term {$ast= $f.ast;}
 	    (PLUS l=term
 	     {$ast= new PlusNode ($ast,$l.ast);}
 	    )*
 	;
 	
term	returns [Node ast]  
	: f=factor {$ast= $f.ast;}
	    (TIMES l=factor
	     {$ast= new TimesNode ($ast,$l.ast);}
	    )*
	;
	
factor	returns [Node ast]  //
	: f=value {$ast= $f.ast;}
	    (EQ l=value 
	     {$ast= new EqualNode ($ast,$l.ast);}
	    )*
 	;	 	
 	          	
value	returns [Node ast]
	: n=INTEGER   
	  {$ast= new IntNode(Integer.parseInt($n.text));}  
	| TRUE 
	  {$ast= new BoolNode(true);}  //
	| FALSE
	  {$ast= new BoolNode(false);}  //
	| LPAR e=exp RPAR
	  {$ast= $e.ast;}  
	| IF x=exp THEN CLPAR y=exp CRPAR  //
		   ELSE CLPAR z=exp CRPAR 
	  {$ast= new IfNode($x.ast,$y.ast,$z.ast);}	 
	| PRINT LPAR e=exp RPAR	//
	  {$ast= new PrintNode($e.ast);} 	
	| i=ID //si aggiunge l'uso della variabile
		{ //cerco la dichiarazione nella tabella <<<<<<<<<<<-------------------
			int j = nestingLevel;
			STentry entry = null; //quando trovo la pallina facendo la ricerca la metto qua
			while (j >= 0 && entry == null) { //cerco l'id nella lista di tabella andando gi� di nesting level
				entry = symTable.get(j--).get($i.text);
			
			} 
			if (entry == null) {
				System.out.println("id "+$i.text+" at line "+$i.line+" not declared");
				System.exit(0);
			}
			$ast = new IdNode($i.text, entry);
		}
	
 	; 

  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/
SEMIC : ';' ;
COLON : ':' ;
COMMA : ',' ;
EQ  : '==' ;
ASS : '=' ;
PLUS  : '+' ;
TIMES : '*' ;
INTEGER : ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE  : 'true' ;
FALSE : 'false' ;
LPAR  : '(' ;
RPAR  : ')' ;
CLPAR   : '{' ;
CRPAR : '}' ;
IF  : 'if' ;
THEN  : 'then' ;
ELSE  : 'else' ;
PRINT : 'print' ; 
LET : 'let' ;
IN  : 'in' ;
VAR : 'var' ;
FUN : 'fun' ;
INT : 'int' ;
BOOL  : 'bool' ;

ID  : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ;

WHITESP : (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;
COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR	    : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 
