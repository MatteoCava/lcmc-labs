package ast;

public class STentry {

	private int nl;
	private Node type;
	private int offset;

	public STentry(int n) {
		nl = n;
	}
	
	public STentry(int n, int offset) {
		this.nl = n;
		this.offset = offset;
	}
	
	public STentry(int nl, Node type, int offset) {
		this.nl = nl;
		this.type = type;
		this.offset = offset;
	}

	public void addType(Node t) {
		type = t;
	}

	public Node getType() {
		return type;
	}

	public int getOffset() {
		return this.offset;
	}
	
	public int getNestingLevel() { 	
		return this.nl;
	}
	
	public String toPrint(String s) {
		return s + "STentry: nestlev " + Integer.toString(nl) + "\n" + 
				s + "STentry: type\n " + type.toPrint(s + "  ") +
				s + "STentry: offset " + offset + "\n";
	}

}