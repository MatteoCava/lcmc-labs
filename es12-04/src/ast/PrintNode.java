package ast;

public class PrintNode implements Node { //stampa quello che c'� sulla cima dello stack mantenendolo inalterato, praticamente ritorna il valore e lo stampa

	private Node exp;

	public PrintNode(Node e) {
		exp = e;
	}

	public String toPrint(String s) {
		return s + "Print\n" + exp.toPrint(s + "  ");
	}

	public Node typeCheck() {
		return exp.typeCheck();
	}

	public String codeGeneration() {
		return exp.codeGeneration() +
				"print\n";
	}

}