package ast;

import lib.FOOLlib;

public class EqualNode implements Node {

	private Node left;
	private Node right;

	public EqualNode(Node l, Node r) {
		left = l;
		right = r;
	}

	public String toPrint(String s) {
		return s + "Equal\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			System.out.println("Incompatible types in equal");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	public String codeGeneration() {
		// qui se uno dei figli � un'eguaglianza mi posso ritrovare delle label ripetute, quindi dobbiamo usare un generatore di label per crearcene sempre di nuove
		final String label1 = FOOLlib.freshLabel();
		final String label2 = FOOLlib.freshLabel();
		
		return left.codeGeneration() +
				right.codeGeneration() +
				"beq " + label1 + "\n" +  //beq poppa i 2 valori sullo stack, contrlla che se sono uguale (stesso valore intero) salta e vuole come argomento un'etichetta
				"push 0\n" + //ritorno false se non sono uguali, quindi pusho 0
				"b " + label2 + //salto incondizionato per non ritornare anche true, si usa branch
				label1 + ": \n" +
				"push 1\n" + 
				label2 + ": \n"; 
				
	}

}