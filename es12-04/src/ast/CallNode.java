package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class CallNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;
	private ArrayList<Node> parlist = new ArrayList<Node>();

	public CallNode(String i, STentry st, ArrayList<Node> p, int nestingLevel) {
		id = i;
		entry = st;
		parlist = p;
		this.nestingLevel = nestingLevel;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}

		return s + "Call:" + id + " at nesting level " + nestingLevel + "\n" 
				+ entry.toPrint(s + "  ") + parlstr;
	}

	public Node typeCheck() {
		ArrowTypeNode t = null;
		if (entry.getType() instanceof ArrowTypeNode)
			t = (ArrowTypeNode) entry.getType();
		else {
			System.out.println("Invocation of a non-function " + id);
			System.exit(0);
		}
		ArrayList<Node> p = t.getParList();
		if (!(p.size() == parlist.size())) {
			System.out.println("Wrong number of parameters in the invocation of " + id);
			System.exit(0);
		}
		for (int i = 0; i < parlist.size(); i++) {
			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
				System.exit(0);
			}
		}
		return t.getRet();
	}

	public String codeGeneration() { 
		String parCodes = "";
		for (int i = parlist.size() - 1; i >= 0; i--) {
			parCodes += parlist.get(i).codeGeneration();
		}
		
		
		//mette tanti lw quanto � la differenza di nesting level 
		String getAR = "";
		for (int i = 0; i < nestingLevel - entry.getNestingLevel() ; i++) {
			getAR += "lw\n";
		}
		
		//se stiamo chiamando una funzione dobbiamo creare l'activation record
		return  "lfp\n" + //settaggio del control link
				parCodes + 	//pushare il risultato dei paramentri in ordine inverso // allocazione valori parametri
				"lfp\n" + getAR +  //ci vuole AL, l'activation record in cui la funzione che sto chiamando � dichiarata // risalgo la catena statica per ottenere l'indirizzo dell'ar in cui � dichiarata la funzione
				
				"push " + this.entry.getOffset() + "\n" + //metto sullo stack offset
				"lfp\n" + getAR + //se la differenza di nesting � > 0 aggiungere tanti lw quanto � la differenza // risalgo la catena statica per ottenere l'indirizzo dell'ar in cui � dichairata la funzioen
				"add\n" + 
				"lw\n" +  // carica sullo stack l'indirizzo a cui saltare 
				"js\n"; //effettuo il salto, come effetto collaterale mette nel registro RA l'indirizzo dell'isntruzione successiva, in modo da poter tornare indietro
	}
	
	//con le ultime 4 righe recupero l'indirizzo a cui saltare

}