package ast;

public class ProgNode implements Node { // radice pi� semplice, caso in cui ho soltanto il corpo senza variabili, una
										// semplice espressione

	private Node exp;

	public ProgNode(Node e) {
		exp = e;
	}

	public String toPrint(String s) {

		return s + "Prog\n" + exp.toPrint(s + "  ");
	}

	public Node typeCheck() {
		return exp.typeCheck();
	}

	public String codeGeneration() {
		return exp.codeGeneration() + 
				"halt\n";
	}

}